const HTTP = require('http');
const PORT = 4000;


let course = [
    {
        "course":"Math",
        
    },
    {
        "course":"Science",
      
    }
];

HTTP.createServer((req, res) => { 
    if(req.url == "/" && req.method == "GET"){
    res.writeHead(200, {"Content-Type": "text/plain"})
    res.end("Welcome to Booking System")

    } else if (req.url == "/profile" && req.method == "GET") { res.writeHead(200, {"Content-Type": "text/plain"})
    res.end("Welcome to your profile!")
    
    } else if (req.url == "/courses" && req.method == "GET") { res.writeHead(200, {"Content-Type": "text/plain"})
    res.end("Here's our courses available")
    

    } else if (req.url == "/addcourse" && req.method == "POST") {

        let reqBody ="";

        req.on("data", (data) => {
            console.log(data)  
            reqBody += data
    })

    req.on("end", () => {  reqBody = JSON.parse(reqBody)     

        res.writeHead(200, {"Content-Type": "text/plain"})
        res.end("Add course to our resources")
    })

    } else if (req.url == "/updatecourse" && req.method == "PUT") {

        let reqBody ="";

        req.on("data", (data) => {
            console.log(data)  
            reqBody += data
    })

    req.on("end", () => {  reqBody = JSON.parse(reqBody) 
        
        
       
        
        course.push(reqBody)

        res.writeHead(200, {"Content-Type": "text/plain"})
        res.write(JSON.stringify(course))
        res.end("Update a course to our resource")
    })

    } else if (req.url == "/archiveCourse" && req.method == "DELETE") {

        let reqBody ="";

        req.on("data", (data) => {
            console.log(data)  
            reqBody += data
    })

    req.on("end", () => {  reqBody = JSON.parse(reqBody)    
        
        course.pop(reqBody)

        res.writeHead(200, {"Content-Type": "text/plain"})
        res.write(JSON.stringify(course))
        res.end("Archive course to our resources")
    
    })
        
    
    }







    }).listen(PORT, () => console.log(`Server connected to port ${PORT}`));